package tictac;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

// ( Single Player )
public class SinglePlayerPane extends Pane {
    
    // 
    Label playerNameLabel = new Label("Player Name");
    TextField playerName = new TextField("player");   
    Button back = new Button("Back");
    Button next = new Button("next");
    
    //
    public SinglePlayerPane() {

        // SinglePlayerPane 
        playerNameLabel.setPrefSize(100, 30);
        playerName.setPrefSize(130, 30);
        back.setPrefSize(240, 40);
        next.setPrefSize(240, 40);

        // SinglePlayerPane
        playerNameLabel.setTranslateX(80);
        playerNameLabel.setTranslateY(170);
        playerName.setTranslateX(190);
        playerName.setTranslateY(170);
        next.setTranslateX(80);
        next.setTranslateY(220);
        back.setTranslateX(80);
        back.setTranslateY(280);
       

        // SinglePlayerPane
        getChildren().add(playerNameLabel);
        getChildren().add(playerName);
        getChildren().add(back);
        getChildren().add(next);
      
    
        
        

   
        back.setOnAction((Action) -> {
            AppManager.viewPane(AppManager.startPane);
            AppManager.gamePane.back.setText("back");
        });
        
        next.setOnAction((Action) -> {
        	if(playerName.getText().isEmpty())
        	{
        		Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("empty name");
                alert.setHeaderText("Results:");
                alert.setContentText("please enter your Name!!");
                alert.showAndWait();
        	}else
        	{
        	AppManager.gamePane.back.setText("change difficulty");
            AppManager.viewPane(AppManager.difficultyofComputer);
            }
        });
        
    }
    
}
