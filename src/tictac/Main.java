package tictac;

import java.io.FileInputStream;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class Main extends Application {
 
	
    private Stage stage;

	@Override
    public void start( Stage stage ) {
    	try { 
    		
        AppManager.setDefaultSettings();
       
     
        Pane root = new Pane();
       
        Scene scene = new Scene(root,400, 500);
       
        HBox box =new HBox();
        box.setMaxSize(400,500); 
        
        
       
        box.setPadding(new Insets(0, 400, 140, 0));
        box.setAlignment(Pos.TOP_CENTER);

        FileInputStream input = new FileInputStream("src/images/backblack.jpg"); 
        
      
        // create a image 
        Image image = new Image(input); 

        // create a background image 
        BackgroundImage backgroundimage = new BackgroundImage(image,  
                                         BackgroundRepeat.SPACE,  
                                         BackgroundRepeat.SPACE,  
                                         BackgroundPosition.DEFAULT,  
                                           BackgroundSize.DEFAULT); 

        // create Background 
        Background background = new Background(backgroundimage); 
        
     
        
        root.setBackground(background); 
        root.getChildren().add(box);
       box.getChildren().add(AppManager.train); 
       box.getChildren().add(AppManager.startPane);
       box.getChildren().add(AppManager.singlePlayerPane);
       box.getChildren().add(AppManager.multiPlayerPane);
       box.getChildren().add(AppManager.settingsPane);
       box.getChildren().add(AppManager.gamePane);
       box.getChildren().add(AppManager.difficultyofComputer);
       box.getChildren().add(AppManager.trainer);
       
        AppManager.viewPane(AppManager.startPane);

  
      box.setStyle(
                "-fx-background-color: rgba(255, 255, 255, 0.5);" +
                "-fx-effect: dropshadow(gaussian, red, 50, 0, 0, 0);" +
                "-fx-background-insets: 70;"+
                "-fx-alignment: center;"
            );
     
      scene.setFill(Color.TRANSPARENT);
       

      
      
 
      
        stage.setTitle("Tic Tac Toe");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        //scene.getStylesheets().addAll(this.getClass().getResource("style.css").toExternalForm());
       
    	} 
    	  
        catch (Exception e) { 
  
            System.out.println(e.getMessage()); 
        } 
    	
     
        
    }

	
    public static void main(String[] args) {
    	//GamePane game = new GamePane(new HumanPlayer() , new HumanPlayer());
        launch(args);
    }

}
