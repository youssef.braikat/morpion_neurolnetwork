package tictac;



import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

// ÙŠÙ…Ø«Ù„ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ Ø³Ù†Ø¸Ù‡Ø±Ù‡Ø§ Ø¹Ù†Ø¯ ØªØ´ØºÙŠÙ„ Ø§Ù„Ø¨Ø±Ù†Ø§Ù…Ø¬ StartPane Ø§Ù„ÙƒÙ„Ø§Ø³ 
public class StartPane extends Pane {

    // Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨Ø¥Ù†Ø´Ø§Ø¡ Ø¬Ù…ÙŠØ¹ Ø§Ù„Ø£Ø´ÙŠØ§Ø¡ Ø§Ù„ØªÙŠ Ø³Ù†Ø¶Ø¹Ù‡Ø§ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ©
    Button singlePlayer = new Button("Single Player");
    Button multiPlayer = new Button("Multi Player");
    Button settings = new Button("Settings");
    Button training = new Button(" Training ");
    Button exit = new Button("Exit");

    // about Ù„Ø£Ù†Ù†Ø§ Ø³Ù†Ø³ØªØ®Ø¯Ù…Ù‡ Ù„Ø¹Ø±Ø¶ Ù†Ø§Ù�Ø°Ø© Ù…Ù†Ø¨Ø«Ù‚Ø© Ø¹Ù†Ø¯Ù…Ø§ ÙŠÙ‚ÙˆÙ… Ø§Ù„Ù…Ø³ØªØ®Ø¯Ù… Ø¨Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø²Ø± Alert Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨Ø¥Ù†Ø´Ø§Ø¡ ÙƒØ§Ø¦Ù† Ù…Ù† Ø§Ù„ÙƒÙ„Ø§Ø³
    Alert alert = new Alert(AlertType.INFORMATION);

    // Ù‡Ø°Ø§ ÙƒÙˆÙ†Ø³ØªØ±ÙƒÙˆØ± Ø§Ù„ÙƒÙ„Ø§Ø³
    public StartPane() {

        // StartPane Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ø­Ø¬Ù… ÙƒÙ„ Ø´ÙŠØ¡ Ø³Ù†Ø¶ÙŠÙ�Ù‡ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† Ø§Ù„Ø°ÙŠ Ù†Ù†Ø´Ø¦Ù‡ Ù…Ù† Ø§Ù„ÙƒÙ„Ø§Ø³
        singlePlayer.setPrefSize(240, 40);
        multiPlayer.setPrefSize(240, 40);
        settings.setPrefSize(240, 40);
        training .setPrefSize(240, 40);
        exit.setPrefSize(170, 40);

        // StartPane Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…ÙˆÙ‚Ø¹ ÙƒÙ„ Ø´ÙŠØ¡ Ø³Ù†Ø¶ÙŠÙ�Ù‡ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† Ø§Ù„Ø°ÙŠ Ù†Ù†Ø´Ø¦Ù‡ Ù…Ù† Ø§Ù„ÙƒÙ„Ø§Ø³
        singlePlayer.setTranslateX(80);
        singlePlayer.setTranslateY(110);
        multiPlayer.setTranslateX(80);
        multiPlayer.setTranslateY(170);
        settings.setTranslateX(80);
        settings.setTranslateY(290);
        training .setTranslateX(80);
        training .setTranslateY(230);
        exit.setTranslateX(20);
        exit.setTranslateY(450);

        // StartPane Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨Ø¥Ø¶Ø§Ù�Ø© ÙƒÙ„ Ø´ÙŠØ¡ Ù‚Ù…Ù†Ø§ Ø¨Ø¥Ù†Ø´Ø§Ø¦Ù‡ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† Ø§Ù„Ø°ÙŠ Ù†Ù†Ø´Ø¦Ù‡ Ù…Ù† Ø§Ù„ÙƒÙ„Ø§Ø³
        getChildren().add(singlePlayer);
        getChildren().add(multiPlayer);
        getChildren().add(settings);
        getChildren().add(training );
        getChildren().add(exit);

        // singlePlayer Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…Ø§ Ø³ÙŠØ­Ø¯Ø« Ø¹Ù†Ø¯ Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø²Ø± Ø§Ù„Ø°ÙŠ ÙŠÙ…Ø«Ù„Ù‡ Ø§Ù„ÙƒØ§Ø¦Ù†
        // Ù…ÙƒØ§Ù† Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„Ø­Ø§Ù„ÙŠØ© singlePlayerPane Ù„Ø¹Ø±Ø¶ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† viewPane() Ø³ÙŠØªÙ… Ø¥Ø³ØªØ¯Ø¹Ø§Ø¡ Ø§Ù„Ø¯Ø§Ù„Ø© Ø§Ù„Ø«Ø§Ø¨ØªØ©
        singlePlayer.setOnAction((Action) -> {
        	
            AppManager.viewPane(AppManager.singlePlayerPane);
        });

        // multiPlayer Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…Ø§ Ø³ÙŠØ­Ø¯Ø« Ø¹Ù†Ø¯ Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø²Ø± Ø§Ù„Ø°ÙŠ ÙŠÙ…Ø«Ù„Ù‡ Ø§Ù„ÙƒØ§Ø¦Ù†
        // Ù…ÙƒØ§Ù† Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„Ø­Ø§Ù„ÙŠØ© multiPlayerPane Ù„Ø¹Ø±Ø¶ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† viewPane() Ø³ÙŠØªÙ… Ø¥Ø³ØªØ¯Ø¹Ø§Ø¡ Ø§Ù„Ø¯Ø§Ù„Ø© Ø§Ù„Ø«Ø§Ø¨ØªØ©
        multiPlayer.setOnAction((Action) -> {
        	//AppManager.gamePane = new GamePane(new HumanPlayer(), new HumanPlayer());
            AppManager.viewPane(AppManager.multiPlayerPane);
        });

        
        settings.setOnAction((Action) -> {
            AppManager.viewPane(AppManager.settingsPane);
        });

        // about Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…Ø§ Ø³ÙŠØ­Ø¯Ø« Ø¹Ù†Ø¯ Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø²Ø± Ø§Ù„Ø°ÙŠ ÙŠÙ…Ø«Ù„Ù‡ Ø§Ù„ÙƒØ§Ø¦Ù†
        // alert Ø³ÙŠØªÙ… ØªØ¬Ù‡ÙŠØ² Ù†Øµ ÙŠÙ…Ø«Ù„ Ù…Ø¹Ù„ÙˆÙ…Ø§Øª Ø¹Ø§Ù…Ø© Ø¹Ù† Ø§Ù„Ù„Ø¹Ø¨Ø© Ùˆ Ø§Ù„Ø°ÙŠ Ø³Ù†Ø¹Ø±Ø¶Ù‡ Ø¨Ø¯Ø§Ø®Ù„ Ø§Ù„Ù†Ø§Ù�Ø°Ø© Ø§Ù„Ù…Ù†Ø¨Ø«Ù‚Ø© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù†
       training .setOnAction((Action) -> {
           
    	   AppManager.viewPane(AppManager.train);
        });

        // exit Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…Ø§ Ø³ÙŠØ­Ø¯Ø« Ø¹Ù†Ø¯ Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø²Ø± Ø§Ù„Ø°ÙŠ ÙŠÙ…Ø«Ù„Ù‡ Ø§Ù„ÙƒØ§Ø¦Ù†
        exit.setOnAction((Action) -> {
            System.exit(0);
        });
    }

}
