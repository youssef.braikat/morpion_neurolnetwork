package tictac;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class DifficultyofComputer extends Pane {
	Label Difficulty = new Label("choose the difficulty");
	Button Novice =new Button(" Novice");
    Button Intermediat = new Button("BoringPlayer");
    Button Expert = new Button("QPlayer");
    Button back = new Button("Back");
	
    public   DifficultyofComputer() {
    	 Difficulty.setPrefSize(200, 30);
    	 Novice.setPrefSize(240, 40);
    	 Intermediat.setPrefSize(240, 40);
    	 Expert.setPrefSize(240, 40);
    	 back.setPrefSize(240, 40);
         
       
    	Difficulty.setTranslateX(110);
    	Difficulty.setTranslateY(90);
    	Novice.setTranslateX(80);
    	Novice.setTranslateY(130);
        Intermediat.setTranslateX(80);
        Intermediat.setTranslateY(190);
        Expert.setTranslateX(80);
        Expert.setTranslateY(250);
        back.setTranslateX(80);
        back.setTranslateY(310);
        
        getChildren().add(Novice);
        getChildren().add(Intermediat);
        getChildren().add(Expert);
        getChildren().add(back);
        getChildren().add(Difficulty);
        
        Novice.setOnAction((Action) -> {
            AppManager.gamePane.firstPlayerName.setText(AppManager.singlePlayerPane.playerName.getText());
            AppManager.gamePane.secondPlayerName.setText("Computer");
            AppManager.gamePane.firstPlayerScore.setText("0");
            AppManager.gamePane.secondPlayerScore.setText("0");
            AppManager.challengeComputer = true;
            AppManager.stupideComputer = true;
           
            
            AppManager.gamePane.boardBackground
                    .setImage(new Image(getClass().getResourceAsStream("/images/"+AppManager.preferredBoard)));
            
            AppManager.viewPane(AppManager.gamePane);
        });
        
       Expert.setOnAction((Action) -> {
            AppManager.gamePane.firstPlayerName.setText(AppManager.singlePlayerPane.playerName.getText());
            AppManager.gamePane.secondPlayerName.setText("Computer");
            AppManager.gamePane.firstPlayerScore.setText("0");
            AppManager.gamePane.secondPlayerScore.setText("0");
            
            AppManager.challengeComputer = true;
            AppManager.challengeQPlayer = true;
            
           
            
            AppManager.gamePane.boardBackground
                    .setImage(new Image(getClass().getResourceAsStream("/images/"+AppManager.preferredBoard)));
            
            AppManager.viewPane(AppManager.gamePane);
        });
       Intermediat.setOnAction((Action) -> {
           AppManager.gamePane.firstPlayerName.setText(AppManager.singlePlayerPane.playerName.getText());
           AppManager.gamePane.secondPlayerName.setText("Computer");
           AppManager.gamePane.firstPlayerScore.setText("0");
           AppManager.gamePane.secondPlayerScore.setText("0");
           
           AppManager.challengeComputer = true;
           AppManager.challengeBoringPlayer = true; 
          
           
           AppManager.gamePane.boardBackground
                   .setImage(new Image(getClass().getResourceAsStream("/images/"+AppManager.preferredBoard)));
           
           AppManager.viewPane(AppManager.gamePane);
       });
        
        
       back.setOnAction((Action) -> {


               AppManager.viewPane(AppManager.singlePlayerPane);
           
       });
        
        
    	
    }
	

}
