package tictac;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;


public class AppManager {
    
    
	static StartPane startPane = new StartPane();
    static SinglePlayerPane singlePlayerPane = new SinglePlayerPane();
    static MultiPlayerPane multiPlayerPane = new MultiPlayerPane();
    static SettingsPane settingsPane = new SettingsPane();
    static TicTacTocGame  gamePane = new TicTacTocGame ();
    static DifficultyofComputer  difficultyofComputer = new DifficultyofComputer() ;
    static IAtraining train=new IAtraining();
    static Trainer trainer=new Trainer();
    static TicTacToe tictactoe=new TicTacToe(null, null);

    // SettingsPane Ø³Ù†Ø®Ø²Ù† Ù�ÙŠÙ‡ Ø¥Ø³Ù… ØµÙˆØ±Ø© Ø®Ù„Ù�ÙŠØ© Ø§Ù„Ù„Ø¹Ø¨Ø© Ø§Ù„ØªÙŠ ÙŠØ³ØªØ·ÙŠØ¹ Ø§Ù„Ù…Ø³ØªØ®Ø¯Ù… ØªØºÙŠÙŠØ±Ù‡Ø§ Ù…Ù† Ø§Ù„Ø­Ø§ÙˆÙŠØ© preferredBoard Ø§Ù„Ù…ØªØºÙŠØ±
    static String preferredBoard;

    // SettingsPane Ø³Ù†Ø®Ø²Ù† Ù�ÙŠÙ‡ Ø­Ø¬Ù… Ø®Ø· ÙƒÙ„ Ø²Ø±, Ù†Øµ Ùˆ Ù…Ø±Ø¨Ø¹ Ù†Øµ Ø£Ø¶Ù�Ù†Ø§Ù‡ Ù�ÙŠ Ø§Ù„Ù„Ø¹Ø¨Ø© Ùˆ Ø§Ù„Ø°ÙŠ ÙŠØ³ØªØ·ÙŠØ¹ Ø§Ù„Ù…Ø³ØªØ®Ø¯Ù… ØªØºÙŠÙŠØ±Ù‡ Ù…Ù† Ø§Ù„Ø­Ø§ÙˆÙŠØ© preferredFont Ø§Ù„ÙƒØ§Ø¦Ù†
    static Font preferredFont;
    public static int row =0;
    public static int colun=0;
    // Ù„Ù„Ø¥Ø´Ø§Ø±Ø© Ø¥Ù„Ù‰ Ø£Ù†Ù‡ Ø³ÙŠØªÙ… Ø§Ù„Ù„Ø¹Ø¨ Ø¶Ø¯ Ø§Ù„ÙƒÙ…Ø¨ÙŠÙˆØªØ± SinglePlayerPane Ø§Ù„Ù…ÙˆØ¶ÙˆØ¹ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ© start Ø¹Ù†Ø¯ Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø²Ø± true Ø³Ù†Ø®Ø²Ù† Ù�ÙŠÙ‡ Ø§Ù„Ù‚ÙŠÙ…Ø© challengeComputer Ø§Ù„Ù…ØªØºÙŠØ±
    static boolean challengeComputer;
    static boolean stupideComputer;
    static boolean challengeQPlayer;
    static boolean challengeBoringPlayer;
    // pane Ø§Ù„Ø¯Ø§Ù„Ø© Ø§Ù„ØªØ§Ù„ÙŠØ© Ù†Ø³ØªØ®Ø¯Ù…Ù‡Ø§ Ù„Ø¥Ø®Ù�Ø§Ø¡ Ø£ÙŠ Ù†Ø§Ù�Ø°Ø© Ù…Ø¹Ø±ÙˆØ¶Ø© Ø­Ø§Ù„ÙŠØ§Ù‹ Ù�ÙŠ Ø§Ù„Ù†Ø§Ù�Ø°Ø© Ùˆ Ø¹Ø±Ø¶ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ Ù†Ù…Ø±Ø±Ù‡Ø§ Ù„Ù‡Ø§ Ù�Ù‚Ø· Ù…ÙƒØ§Ù† Ø§Ù„Ø¨Ø§Ø±Ø§Ù…ÙŠØªØ±
    public static void viewPane(Pane pane)
    {
        startPane.setVisible(false);
        singlePlayerPane.setVisible(false);
        multiPlayerPane.setVisible(false);
        settingsPane.setVisible(false);
        gamePane.setVisible(false);
        difficultyofComputer.setVisible(false);
        train.setVisible(false);
        trainer.setVisible(false);
        pane.setVisible(true);
        
        
    }
    
    // settingsPane Ø§Ù„Ø¯Ø§Ù„Ø© Ø§Ù„ØªØ§Ù„ÙŠØ© Ù†Ø³ØªØ®Ø¯Ù…Ù‡Ø§ Ù„ÙˆØ¶Ø¹ Ø§Ù„Ø®ÙŠØ§Ø±Ø§Øª Ø§Ù„Ø¥Ù�ØªØ±Ø§Ø¶ÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…ÙƒÙ† ØªØºÙŠÙŠØ±Ù‡Ø§ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ©
    public static void setDefaultSettings()
    {
        // fontSizesComboBox Ùˆ Ø«Ø§Ù†ÙŠ Ø®ÙŠØ§Ø± Ù�ÙŠ Ø§Ù„ÙƒØ§Ø¦Ù† boardsComboBox Ù‡Ù†Ø§ Ù‚Ù„Ù†Ø§ Ø£Ù†Ù‡ Ø³ÙŠØªÙ… Ø¥Ø®ØªÙŠØ§Ø± Ø£ÙˆÙ„ Ø®ÙŠØ§Ø± Ù�ÙŠ Ø§Ù„ÙƒØ§Ø¦Ù†
        settingsPane.boardsComboBox.getSelectionModel().selectFirst();
        settingsPane.fontSizesComboBox.getSelectionModel().select(1);
        
        // preferredFont Ù„ØªØºÙŠÙŠØ± Ø­Ø¬Ù… Ø®Ø· ÙƒÙ„ Ø²Ø±, Ù†Øµ Ùˆ Ù…Ø±Ø¨Ø¹ Ù†Øµ Ù…ÙˆØ¶ÙˆØ¹ Ù�ÙŠ Ø§Ù„Ù„Ø¹Ø¨Ø© Ù†Ø³Ø¨Ø©Ù‹ Ù„Ù‚ÙŠÙ…Ø© Ø§Ù„ÙƒØ§Ø¦Ù† setFont() Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨Ø§Ø³ØªØ¯Ø¹Ø§Ø¡ Ø§Ù„Ø¯Ø§Ù„Ø©
        setFont();
    }

    // preferredFont Ø§Ù„Ø¯Ø§Ù„Ø© Ø§Ù„ØªØ§Ù„ÙŠØ© Ù†Ø³ØªØ®Ø¯Ù…Ù‡Ø§ Ù„ØªØ­Ø¯ÙŠØ¯ Ø­Ø¬Ù… Ø®Ø· ÙƒÙ„ Ø²Ø±, Ù†Øµ Ùˆ Ù…Ø±Ø¨Ø¹ Ù†Øµ Ù…ÙˆØ¶ÙˆØ¹ Ù�ÙŠ Ø§Ù„Ù„Ø¹Ø¨Ø© Ù†Ø³Ø¨Ø©Ù‹ Ù„Ù‚ÙŠÙ…Ø© Ø§Ù„ÙƒØ§Ø¦Ù†
    public static void setFont()
    {
        startPane.singlePlayer.setFont(preferredFont);
        startPane.multiPlayer.setFont(preferredFont);
        startPane.settings.setFont(preferredFont);
        startPane.training .setFont(preferredFont);
        startPane.exit.setFont(preferredFont);
        
        singlePlayerPane.playerNameLabel.setFont(preferredFont);
        singlePlayerPane.playerName.setFont(preferredFont);
        singlePlayerPane.back.setFont(preferredFont);
        singlePlayerPane.next.setFont(preferredFont);
        
        multiPlayerPane.playerXLabel.setFont(preferredFont);
        multiPlayerPane.playerOLabel.setFont(preferredFont);
        multiPlayerPane.firstPlayerName.setFont(preferredFont);
        multiPlayerPane.secondPlayerName.setFont(preferredFont);
        multiPlayerPane.start.setFont(preferredFont);
        multiPlayerPane.back.setFont(preferredFont);
        
        gamePane.firstPlayerName.setFont(preferredFont);
        gamePane.secondPlayerName.setFont(preferredFont);
        gamePane.firstPlayerScore.setFont(preferredFont);
        gamePane.secondPlayerScore.setFont(preferredFont);
        gamePane.currentPlayerSymbol.setFont(preferredFont);
        gamePane.newGame.setFont(preferredFont);
        gamePane.back.setFont(preferredFont);
    
        
        settingsPane.labelForBoards.setFont(preferredFont);
        settingsPane.labelForFontSizes.setFont(preferredFont);
        settingsPane.reset.setFont(preferredFont);
        settingsPane.back.setFont(preferredFont);
        
        difficultyofComputer.Novice.setFont(preferredFont);
        difficultyofComputer.Intermediat.setFont(preferredFont);
        difficultyofComputer.Expert.setFont(preferredFont);
        difficultyofComputer.back.setFont(preferredFont);
        difficultyofComputer.Difficulty.setFont(preferredFont);
        
        train.HUMANPLAYERvsHUMANPLAYER.setFont(preferredFont);
        train.QPLYERvsHUMANPLAYER.setFont(preferredFont);
        train.QPLYERvsRandomPlyer.setFont(preferredFont);
        train.Way.setFont(preferredFont);
        train.back.setFont(preferredFont);
        
        trainer.textField.setFont(preferredFont);
        trainer.textField3.setFont(preferredFont);
       trainer.textArea.setFont(preferredFont);
       trainer.back.setFont(preferredFont);
       trainer.lan.setFont(preferredFont);
        
        // Ù„ØªØ­Ø¯ÙŠØ¯ Ù„Ù‡Ù…Ø§ setStyle() Ù„Ø§ ÙŠÙ…Ù„ÙƒØ§Ù† Ø¯Ø§Ù„Ø© Ø®Ø§ØµØ© Ù„ØªØ­Ø¯ÙŠØ¯ Ø­Ø¬Ù… Ø§Ù„Ø®Ø·, Ù„Ø°Ù„Ùƒ Ù‚Ù…Ù†Ø§ Ø¨Ø§Ø³ØªØ®Ø¯Ø§Ù… Ø§Ù„Ø¯Ø§Ù„Ø© fontSizesComboBox Ùˆ boardsComboBox Ø§Ù„ÙƒØ§Ø¦Ù†ÙŠÙ†
        settingsPane.boardsComboBox.setStyle(
            "-fx-font-family:" + preferredFont.getName() + ";"
           +"-fx-font-size: " + preferredFont.getSize() +"px;"
           +"-fx-font-weight: bold;"
        );
        settingsPane.fontSizesComboBox.setStyle(
            "-fx-font-family:" + preferredFont.getName() + ";"
           +"-fx-font-size: " + preferredFont.getSize() +"px;"
           +"-fx-font-weight: bold;"
        );
        
    }
    
}
