package tictac;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

// ( Settings ) ÙŠÙ…Ø«Ù„ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ Ø³Ù†Ø¸Ù‡Ø±Ù‡Ø§ Ø¹Ù†Ø¯ Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø²Ø± Ø¶Ø¨Ø· Ø§Ù„Ù„Ø¹Ø¨Ø© SettingsPane Ø§Ù„ÙƒÙ„Ø§Ø³ 
public class SettingsPane extends Pane {

    // Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨Ø¥Ù†Ø´Ø§Ø¡ Ø¬Ù…ÙŠØ¹ Ø§Ù„Ø£Ø´ÙŠØ§Ø¡ Ø§Ù„ØªÙŠ Ø³Ù†Ø¶Ø¹Ù‡Ø§ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ©
    Label labelForBoards = new Label("Game Board");
    Label labelForFontSizes = new Label("Font Size");
    ComboBox boardsComboBox = new ComboBox();
    ComboBox fontSizesComboBox = new ComboBox();
    Button reset = new Button("Reset Default Settings");
    Button back = new Button("Back");

    // Ù‡Ø°Ø§ ÙƒÙˆÙ†Ø³ØªØ±ÙƒÙˆØ± Ø§Ù„ÙƒÙ„Ø§Ø³
    public SettingsPane() {

        // fontSizesComboBox Ùˆ boardsComboBox Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ÙˆØ¶Ø¹ Ø§Ù„Ø®ÙŠØ§Ø±Ø§Øª Ø§Ù„ØªÙŠ ÙŠÙ…ÙƒÙ† Ù„Ù„Ù…Ø³ØªØ®Ø¯Ù… Ø§Ø®ØªÙŠØ§Ø±Ù‡Ø§ Ù�ÙŠ Ø§Ù„ÙƒØ§Ø¦Ù†ÙŠÙ†
        boardsComboBox.getItems().addAll("Board 1", "Board 2", "Board 3", "Board 4");
        fontSizesComboBox.getItems().addAll("Small", "Medium", "Large");

        // SettingsPane Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ø­Ø¬Ù… ÙƒÙ„ Ø´ÙŠØ¡ Ø³Ù†Ø¶ÙŠÙ�Ù‡ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† Ø§Ù„Ø°ÙŠ Ù†Ù†Ø´Ø¦Ù‡ Ù…Ù† Ø§Ù„ÙƒÙ„Ø§Ø³
        labelForBoards.setPrefSize(100, 30);
        boardsComboBox.setPrefSize(120, 30);
        labelForFontSizes.setPrefSize(100, 30);
        fontSizesComboBox.setPrefSize(120, 30);
        reset.setPrefSize(240, 40);
        back.setPrefSize(240, 40);

        // SettingsPane Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…ÙˆÙ‚Ø¹ ÙƒÙ„ Ø´ÙŠØ¡ Ø³Ù†Ø¶ÙŠÙ�Ù‡ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† Ø§Ù„Ø°ÙŠ Ù†Ù†Ø´Ø¦Ù‡ Ù…Ù† Ø§Ù„ÙƒÙ„Ø§Ø³
        labelForBoards.setTranslateX(80);
        labelForBoards.setTranslateY(130);
        boardsComboBox.setTranslateX(200);
        boardsComboBox.setTranslateY(130);
        labelForFontSizes.setTranslateX(80);
        labelForFontSizes.setTranslateY(190);
        fontSizesComboBox.setTranslateX(200);
        fontSizesComboBox.setTranslateY(190);
        reset.setTranslateX(80);
        reset.setTranslateY(250);
        back.setTranslateX(80);
        back.setTranslateY(310);
 
        // SettingsPane Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨Ø¥Ø¶Ø§Ù�Ø© ÙƒÙ„ Ø´ÙŠØ¡ Ù‚Ù…Ù†Ø§ Ø¨Ø¥Ù†Ø´Ø§Ø¦Ù‡ Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† Ø§Ù„Ø°ÙŠ Ù†Ù†Ø´Ø¦Ù‡ Ù…Ù† Ø§Ù„ÙƒÙ„Ø§Ø³       
        getChildren().add(labelForBoards);
        getChildren().add(boardsComboBox);
        getChildren().add(labelForFontSizes);
        getChildren().add(fontSizesComboBox);
        getChildren().add(reset);
        getChildren().add(back);
        
        // boardsComboBox Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…Ø§ Ø³ÙŠØ­Ø¯Ø« Ø¹Ù†Ø¯Ù…Ø§ ÙŠÙ‚ÙˆÙ… Ø§Ù„Ù…Ø³ØªØ®Ø¯Ù… Ø¨ØªØºÙŠÙŠØ± Ø§Ù„Ù‚ÙŠÙ…Ø© Ø§Ù„Ø¸Ø§Ù‡Ø±Ø© Ù�ÙŠ Ø§Ù„ÙƒØ§Ø¦Ù†
        // AppManager Ø§Ù„Ù…ÙˆØ¶ÙˆØ¹ Ù�ÙŠ Ø§Ù„ÙƒÙ„Ø§Ø³ preferredBoard Ø¨Ù†Ø§Ø¡Ø§Ù‹ Ø¹Ù„Ù‰ Ø§Ù„Ù‚ÙŠÙ…Ø© Ø§Ù„ØªÙŠ ÙŠØ®ØªØ§Ø±Ù‡Ø§ Ø³ÙŠØªÙ… ØªÙ…Ø±ÙŠØ± Ø¥Ø³Ù… Ø§Ù„ØµÙˆØ±Ø© Ù„Ù„Ù…ØªØºÙŠØ± Ø§Ù„Ø«Ø§Ø¨Øª
        boardsComboBox.getSelectionModel().selectedIndexProperty().addListener(
            (ObservableValue<? extends Number> ov, Number oldVal, Number newVal) -> {
                switch((int)newVal) {
                    case 0:
                        AppManager.preferredBoard = "board_1.png";
                        break;
                        
                    case 1:
                        AppManager.preferredBoard = "board_2.png";
                        break;
                        
                    case 2:
                        AppManager.preferredBoard = "board_3.png";
                        break;
                        
                    case 3:
                        AppManager.preferredBoard = "board_4.png";
                        break;
                }
        });
 
        // fontSizesComboBox Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…Ø§ Ø³ÙŠØ­Ø¯Ø« Ø¹Ù†Ø¯Ù…Ø§ ÙŠÙ‚ÙˆÙ… Ø§Ù„Ù…Ø³ØªØ®Ø¯Ù… Ø¨ØªØºÙŠÙŠØ± Ø§Ù„Ù‚ÙŠÙ…Ø© Ø§Ù„Ø¸Ø§Ù‡Ø±Ø© Ù�ÙŠ Ø§Ù„ÙƒØ§Ø¦Ù†
        // AppManager Ø§Ù„Ù…ÙˆØ¶ÙˆØ¹ Ù�ÙŠ Ø§Ù„ÙƒÙ„Ø§Ø³ preferredFont Ø¨Ù†Ø§Ø¡Ø§Ù‹ Ø¹Ù„Ù‰ Ø§Ù„Ù‚ÙŠÙ…Ø© Ø§Ù„ØªÙŠ ÙŠØ®ØªØ§Ø±Ù‡Ø§ Ø³ÙŠØªÙ… ØªÙ…Ø±ÙŠØ± Ø­Ø¬Ù… Ø§Ù„Ø®Ø· Ù„Ù„ÙƒØ§Ø¦Ù† Ø§Ù„Ø«Ø§Ø¨Øª
        // Ù„ØªØºÙŠÙŠØ± Ø­Ø¬Ù… Ø®Ø· ÙƒÙ„ Ø§Ù„Ø£Ø²Ø±Ø§Ø±, Ø§Ù„Ù†ØµÙˆØµ Ùˆ Ù…Ø±Ø¨Ø¹Ø§Øª Ø§Ù„Ù†ØµÙˆØµ Ø§Ù„Ù…ÙˆØ¶ÙˆØ¹Ø© Ù�ÙŠ Ø§Ù„Ù„Ø¹Ø¨Ø© setFont() ÙƒÙ…Ø§ Ø£Ù†Ù‡ Ø³ÙŠØªÙ… Ø§Ø³ØªØ¯Ø¹Ø§Ø¡ Ø§Ù„Ø¯Ø§Ù„Ø©
        fontSizesComboBox.getSelectionModel().selectedIndexProperty().addListener(
            (ObservableValue<? extends Number> ov, Number oldVal, Number newVal) -> {
                
            String selectedFont = fontSizesComboBox.getSelectionModel().getSelectedItem().toString();
            int fontSize = 0;

            switch(selectedFont) {
                case "Small":
                    fontSize = 15;
                    break;
                
                case "Medium":
                    fontSize = 16;
                    break;
                    
                case "Large":
                    fontSize = 17;
                    break;
            }
            
            AppManager.preferredFont = Font.font("Arial", FontWeight.BOLD, fontSize);
            AppManager.setFont();

        });
        
        // reset Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…Ø§ Ø³ÙŠØ­Ø¯Ø« Ø¹Ù†Ø¯ Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø²Ø± Ø§Ù„Ø°ÙŠ ÙŠÙ…Ø«Ù„Ù‡ Ø§Ù„ÙƒØ§Ø¦Ù†
        // Ù„Ø¥Ø±Ø¬Ø§Ø¹ Ø§Ù„Ù‚ÙŠÙ… Ø§Ù„Ø¥Ù�ØªØ±Ø§Ø¶ÙŠØ© Ø§Ù„ØªÙŠ ÙƒØ§Ù†Øª Ù…ÙˆØ¶ÙˆØ¹Ø© Ù�ÙŠ Ø§Ù„Ø­Ø§ÙˆÙŠØ© AppManager Ø§Ù„Ù…ÙˆØ¬ÙˆØ¯Ø© Ù�ÙŠ Ø§Ù„ÙƒÙ„Ø§Ø³ setDefaultSettings() Ø³ÙŠØªÙ… Ø§Ø³ØªØ¯Ø¹Ø§Ø¡ Ø§Ù„Ø¯Ø§Ù„Ø©
        reset.setOnAction((Action) -> {
            AppManager.setDefaultSettings();
            boardsComboBox.getSelectionModel().selectFirst();
            fontSizesComboBox.getSelectionModel().select(1);
        });
        
        // back Ù‡Ù†Ø§ Ù‚Ù…Ù†Ø§ Ø¨ØªØ­Ø¯ÙŠØ¯ Ù…Ø§ Ø³ÙŠØ­Ø¯Ø« Ø¹Ù†Ø¯ Ø§Ù„Ù†Ù‚Ø± Ø¹Ù„Ù‰ Ø§Ù„Ø²Ø± Ø§Ù„Ø°ÙŠ ÙŠÙ…Ø«Ù„Ù‡ Ø§Ù„ÙƒØ§Ø¦Ù†
        // Ù…ÙƒØ§Ù† Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„Ø­Ø§Ù„ÙŠØ© startPane Ù„Ø¹Ø±Ø¶ Ø§Ù„Ø­Ø§ÙˆÙŠØ© Ø§Ù„ØªÙŠ ÙŠÙ…Ø«Ù„Ù‡Ø§ Ø§Ù„ÙƒØ§Ø¦Ù† viewPane() Ø³ÙŠØªÙ… Ø¥Ø³ØªØ¯Ø¹Ø§Ø¡ Ø§Ù„Ø¯Ø§Ù„Ø© Ø§Ù„Ø«Ø§Ø¨ØªØ©
        back.setOnAction((Action) -> {
            AppManager.viewPane(AppManager.startPane);
        });
        
    }

}
