package tictac;

import java.util.Date;

import org.encog.util.logging.Logging;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
/*
public class Trainer extends Application {
	 public void start(Stage primaryStage) throws Exception {
		Logging.stopConsoleLogging();
		TicTacToeBenchmark benchmark = new TicTacToeBenchmark(100, new RandomPlayer());
		TextField textField = new TextField();
        textField.setMinWidth(600);
		QPlayer p1 = new QPlayer("P1");
		Player p2 = new RandomPlayer();//new QPlayer("P2");
		Player p = new RandomPlayer();
		
		p1.setLearn(false);
		textField.setText("Score before training: " + benchmark.calculateScore(p1));
		p1.setLearn(true);
		
		// training games
		for (int i = 1; i <= 1; i++) {
			TicTacToe game = new TicTacToe(p, p2);
			game.start();
			
			if (i % 10 == 0) {
				p1.setLearn(false);
				textField.setText("Score after " + i + " training games: " + benchmark.calculateScore(p1));
			//ystem.out.println("Score after " + i + " training games: " + benchmark.calculateScore(p1));
				p1.setLearn(true);
			}
		}
		
		p1.setLearn(false);
		textField.setText("Score after training: " + benchmark.calculateScore(p1));
		p1.setLearn(true);
		
		TicTacToe ticTacToe = new TicTacToe(p1, new HumanPlayer());
		ticTacToe.start();
		

        FlowPane root = new FlowPane();
        root.setPadding(new Insets(10));
        root.setVgap(5);
        root.setHgap(5);
 
        root.getChildren().addAll(textField
               );
 
        Scene scene = new Scene(root, 200, 100);
 
        primaryStage.setTitle("JavaFX TextField (o7planning.org)");
        primaryStage.setScene(scene);
        primaryStage.show();
	}*/
import tictactoe.Player;
import tictactoe.QPlayer;
import tictactoe.RandomPlayer;
import tictactoe.TicTacToeBenchmark;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
 
public class  Trainer extends Pane  {
 
    

	 TextField textField = new TextField();
	 TextArea textArea = new TextArea();
     TextField textField3 = new TextField();
     HBox buttonBar1 = new HBox();
     Button buttonAppend = new Button("Append Text");
     Button  back = new Button(" back");
     Button  lan = new Button("laner l'aprentissage");
	public Trainer () {
		textField3.setMinWidth(300);
		textField.setMinWidth(300);
		 back.setPrefSize(140,40);
		 lan.setPrefSize(140,40);
	      
		 textField.setTranslateX(50);
	        textField.setTranslateY(10);
	        textField3.setTranslateX(50);  
	        textField3.setTranslateY(380);
	        textArea .setTranslateY(60);
	        textArea.setMinHeight(300);
	        back.setTranslateX(20);
	        back.setTranslateY(450);
	lan.setTranslateX(230);
	lan.setTranslateY(455);
    	Logging.stopConsoleLogging();
		TicTacToeBenchmark benchmark = new TicTacToeBenchmark(100, new RandomPlayer());
		QPlayer p1 = new QPlayer("P1");
		QPlayer p2 = new QPlayer("p2");//new QPlayer("P2");
		Player p = new RandomPlayer();
		p1.setLearn(false);
         textField.setText("Score before training: " + benchmark.calculateScore(p1)+"\n");
       
        
       
        p1.setLearn(true);
     
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                textArea.selectRange(6, 9);
            }
        });
        
       
        //HBox buttonBar = new HBox();
        getChildren().add(textField);
        getChildren().add(textField3);
        
       getChildren().add(textArea );
       getChildren().add( back);
       getChildren().add( lan);
      
        //buttonBar1.getChildren().addAll(buttonAppend);
 
        
        
       
        // Action event.
      
       
       lan.setOnAction(new EventHandler<ActionEvent>() {
 
            public void handle(ActionEvent event) {
     		for (int i = 1; i <=200; i++) {
     			TicTacToe game = new TicTacToe(p, p2);
     			game.start();
     			
     		
     				p1.setLearn(false);
     				textArea.appendText("Score after " + i + " training games: " + benchmark.calculateScore(p1));
                    textArea.appendText("\n");
     				
     				p1.setLearn(true);
     			
     			
     			
     		}
     		
     		 p1.setLearn(false);
     		textField3.setText("Score after training: " + benchmark.calculateScore(p1));
     		p1.setLearn(true);
     		
     		
          }
      });
     		
     		
     		 back.setOnAction((Action) -> {


                 AppManager.viewPane(AppManager.train);
             
         });
     	
    }
 
 
}