package tictac;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public class IAtraining extends Pane  {

	Label Way = new Label("choose the way of learning");
	Button QPLYERvsHUMANPLAYER=new Button("Human vs Qplyer");
    Button QPLYERvsRandomPlyer = new Button(" AI Fast learning  ");
    Button HUMANPLAYERvsHUMANPLAYER = new Button("Human vs Human ");
    Button back = new Button("Back");
	
    public IAtraining() {
    	Way.setPrefSize(200, 30);
    	QPLYERvsHUMANPLAYER.setPrefSize(240,40);
    	QPLYERvsRandomPlyer.setPrefSize(240,40);
    	HUMANPLAYERvsHUMANPLAYER.setPrefSize(240, 40);
    	back.setPrefSize(240, 40);
         
    	Way.setTranslateX(110);
    	Way.setTranslateY(90);
    	 QPLYERvsHUMANPLAYER.setTranslateX(80);
    	 QPLYERvsHUMANPLAYER.setTranslateY(130);
    	 QPLYERvsRandomPlyer.setTranslateX(80);
    	 QPLYERvsRandomPlyer.setTranslateY(190);
    	 HUMANPLAYERvsHUMANPLAYER.setTranslateX(80);
    	 HUMANPLAYERvsHUMANPLAYER.setTranslateY(250);
       
         back.setTranslateX(80);
         back.setTranslateY(310);
        
        getChildren().add(QPLYERvsHUMANPLAYER);
        getChildren().add(QPLYERvsRandomPlyer);
        getChildren().add(HUMANPLAYERvsHUMANPLAYER);
        getChildren().add(back);
        getChildren().add(Way);
        
        QPLYERvsHUMANPLAYER.setOnAction((Action) -> {
        	
        
        });
        
        QPLYERvsRandomPlyer.setOnAction((Action) -> {
        	AppManager.viewPane(AppManager.trainer);
            
          
        });
        
        
       back.setOnAction((Action) -> {


               AppManager.viewPane(AppManager.singlePlayerPane);
           
       });
        
        
    	
    }
	
	
	
}
