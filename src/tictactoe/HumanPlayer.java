package tictactoe;

import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import tictac.Move;
import tictac.TicTacToe;

public class HumanPlayer implements Player {

	@Override
	public String getName() {
		return "Human";
	}

	@Override
	public Move doMove(TicTacToe game) {
		System.out.print(game.toString());
		Scanner in = new Scanner(System.in);
		System.out.println("Enter row:");
		int x = in.nextInt();
		System.out.println("Enter column:");
		int y = in.nextInt();
		return new Move(x, y);
	}
	
	public Move Action(ActionEvent e)  {
		 Button clickedButton = (Button) e.getSource();
	     int X=GridPane.getRowIndex(clickedButton) ;  
	     int Y=GridPane.getColumnIndex(clickedButton) ;  
		return new Move(X,Y);
		
	
	
	
	}


	@Override
	public void onGameOver(TicTacToe game) {
		System.out.print(game.toString());

		if (game.getWinner() == this)
			System.out.println("You won!!");
		else if (game.isDraw())
			System.out.println("Draw");
		else
			System.out.println("You lost!!");
	}

}
