package tictactoe;

import javafx.event.ActionEvent;
import tictac.Move;
import tictac.TicTacToe;

public interface Player {

	public String getName();
	public Move doMove(TicTacToe game);
	public void onGameOver(TicTacToe game);
	public Move Action(ActionEvent e) ;
}
